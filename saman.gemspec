# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'bank_gateway_saman'
  s.version     = '1.0.3'
  s.date        = '2010-06-06'
  s.summary     = 'Help you to communicate better with saman payment gateway'
  s.description = 'Help you to handle all requests to saman payment gateway'
  s.authors     = ['Amirhosein Zolfaghari']
  s.email       = 'amirhosein@zolfaghari.me'
  s.files       = Dir['lib/**/*.rb']
  s.homepage    = 'https://gitlab.com/amirhosein.zlf/bank_gateway_saman'
  s.license       = 'MIT'
  s.require_paths = ['lib']
  s.add_runtime_dependency 'rest-client', '~> 2.0.2', '>= 2.0.2'
  s.add_runtime_dependency 'savon', '~> 2.12.0', '>= 2.12.0'
  s.required_ruby_version = '~> 2.4'
  s.extra_rdoc_files = ['README.md']
end
