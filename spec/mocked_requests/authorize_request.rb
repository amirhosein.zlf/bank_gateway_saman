def mock_authorize_request
  response = '{"status":1,"token":"this is your token"}'
  stub_request(:post, Saman.configuration.authorize_address).
    with(
      body: "{\"Action\":\"Token\",\"Amount\":3000,\"Wage\":0,\"TerminalId\":\"terminal_id_default\",\"ResNum\":123456,\"RedirectUrl\":\"http://localhost\",\"CellNumber\":\"09121234567\"}",
      headers: {
        'Content-Type'=>'application/json',
      }).
    to_return(status: 200, body: response, headers: {})
end

def mock_authorize_request_wage_only
  response = '{"status":1,"token":"this is your token"}'
  stub_request(:post, Saman.configuration.authorize_address).
    with(
      body: "{\"Action\":\"Token\",\"Amount\":3000,\"Wage\":0,\"TerminalId\":\"terminal_id_wage_only\",\"ResNum\":123456,\"RedirectUrl\":\"http://localhost\",\"CellNumber\":\"09121234567\"}",
      headers: {
        'Content-Type'=>'application/json',
      }).
    to_return(status: 200, body: response, headers: {})
end

def mock_authorize_request_for_multiple_accounts
  response = '{"status":1,"token":"this is your token for multiple accounts"}'
  stub_request(:post, Saman.configuration.authorize_address).
    with(
      body: "{\"Action\":\"Token\",\"Amount\":1200,\"Wage\":1800,\"TerminalId\":\"terminal_id_with_wage\",\"ResNum\":123456,\"RedirectUrl\":\"http://localhost\",\"CellNumber\":\"09121234567\"}",
      headers: {
        'Content-Type'=>'application/json',
      }).
    to_return(status: 200, body: response, headers: {})
end

def mock_authorize_request_unsuccessful
  response = '{"status":-1,"errorCode":"5","errorDesc":"Something went wrong!"}'
  stub_request(:post, Saman.configuration.authorize_address).
    with(
      body: "{\"Action\":\"Token\",\"Amount\":3000,\"Wage\":0,\"TerminalId\":\"terminal_id_default\",\"ResNum\":123456,\"RedirectUrl\":\"http://localhost\",\"CellNumber\":\"09121234567\"}",
      headers: {
        'Content-Type'=>'application/json',
      }).
    to_return(status: 200, body: response, headers: {})
end

def mock_authorize_request_timeout
  stub_request(:post, Saman.configuration.authorize_address).to_timeout
end

def mock_authorize_request_general_exception
  stub_request(:post, Saman.configuration.authorize_address).
    to_raise('My Error!!!')
end