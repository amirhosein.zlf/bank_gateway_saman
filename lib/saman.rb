# frozen_string_literal: true

require_relative 'saman/authorize'
require_relative 'saman/verify'
require_relative 'saman/health_check'
require_relative 'saman/configuration'
require_relative 'generators/saman/install_generator'

require 'savon'
require 'yaml'
require 'rest-client'
require 'json'

# Saman gateway module to handle requests
module Saman
  attr_accessor :config

  def self.authorize(params)
    Saman::Client.new.authorize(params)
  end

  def self.verify(params)
    Saman::Client.new.verify(params)
  end

  def self.up?
    Saman::Client.new.health_check
  end
end
