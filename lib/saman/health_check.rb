# frozen_string_literal: true

module Saman
  # Client class to handle requests
  class Client
    def health_check
      authorize_up? && verify_up?
    end

    def verify_up?
      verify_client = Savon.client(wsdl: Saman.configuration.verify_wsdl)
      expected_operations_for_verify = %i[
        verify_transaction
        verify_transaction1
      ]

      (expected_operations_for_verify - verify_client.operations).empty?
    rescue StandardError
      return false
    end

    def authorize_up?
      authorize_client = Savon.client(wsdl: Saman.configuration.authorize_wsdl)
      expected_operations_for_authorize = %i[
        request_token
        request_multi_settle_type_token
      ]

      (expected_operations_for_authorize - authorize_client.operations).empty?
    rescue StandardError
      return false
    end
  end
end
