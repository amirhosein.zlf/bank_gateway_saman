# frozen_string_literal: true

# Saman module for gateway communications
module Saman
  # extend Client class to handle authorize method
  class Client
    def authorize(params)
      config = Saman.configuration
      request_parameters = create_authorize_parameters(params)
      response = send_rest_requests(
        config.authorize_address,
        request_parameters,
        config.retry_count
      )
      raise response[:error] unless response.is_a? RestClient::Response
      authorize_result(response)
    end

    private

    def authorize_result(response)
      token = parse_authorize_result(response)
      {
        method: 'POST',
        fields: {
          Token: token
        },
        url: Saman.configuration.authorize_address
      }
    end

    def create_authorize_parameters(params)
      {
        Action: 'Token',
        Amount: calculate_the_amount(params),
        Wage: calculate_the_wage(params),
        TerminalId: get_terminal_id(params),
        ResNum: params[:order_id],
        RedirectUrl: params[:redirect_url],
        CellNumber: params[:customer][:mobile_number]
      }
    end

    def get_terminal_id(params)
      case params[:split_amount]&.count
      when nil
        Saman.configuration.terminal_id_default
      when 1
        return Saman.configuration.terminal_id_wage_only if \
                                              params[:split_amount].key?(:wage)
        Saman.configuration.terminal_id_default
      else
        Saman.configuration.terminal_id_with_wage
      end
    end

    def calculate_the_wage(params)
      return 0 unless multiple_accounts?(params)
      return 0 if params[:split_amount].count == 1

      params[:split_amount][:wage] || 0
    end

    def calculate_the_amount(params)
      return params[:amount] unless multiple_accounts?(params)
      return params[:amount] if params[:split_amount].count == 1

      params[:split_amount][:amount] || 0
    end

    def multiple_accounts?(params)
      params[:split_amount]&.count&.positive?
    end

    def parse_authorize_result(response)
      token_result = JSON.parse(response.body)
      token = token_result['token'] if token_result['status'].to_i == 1
      raise "Code ##{token_result['errorCode']}, #{token_result['errorDesc']}" \
        if token.nil?

      token
    end

    def send_rest_requests(url, parameters, retry_to)
      RestClient.proxy = Saman.configuration.proxy unless \
        Saman.configuration.proxy.blank?
      return RestClient.post(url, parameters.to_json, content_type: :json)
    rescue RestClient::Exceptions::OpenTimeout => exception
      retry if (retry_to -= 1).positive?
      return { error: 'Saman is not available right now,
                                            calling web service got time out' }
    rescue StandardError => exception
      retry if (retry_to -= 1).positive?
      return { error: exception.message }
    end
  end
end
